# Imports ======================================================================

import pandas as pd
from snakemake.utils import validate
from math import floor




# Config =======================================================================

validate(config, "config/config.schema.json")

chromsizes = pd.read_table(config["chrom_sizes"], header=None, index_col=0)
large_contigs = [contig for contig, row in chromsizes[:10].iterrows()]
target_plots = expand(config["output_prefix"] + "/{contig}.svg", contig=large_contigs)
target_tables = expand(config["output_prefix"] + "/{contig}.tsv", contig=large_contigs)
target_bedgraphs = expand(config["output_prefix"] + "/{contig}_{parent}.bdg.gz",
                        contig=large_contigs, parent=config["groups"])
target_bigwigs = expand(config["output_prefix"] + "/{contig}_{parent}.bw",
                        contig=large_contigs, parent=config["groups"])




# Functions ====================================================================

def get_mem_mb(wildcards, attempt):
    contig_size = chromsizes.loc[wildcards.contig, 1]
    return 2**(floor(attempt + contig_size/50e6)) * 10_000 * len(config["indexes"])


def get_disk_mb(wildcards, attempt):
    contig_size = chromsizes.loc[wildcards.contig, 1]
    return 2**(floor(attempt + contig_size/50e6)) * 10_000 * len(config["indexes"])


def get_contig_size(wildcards):
    return chromsizes.loc[wildcards.contig, 1]




# Rules ========================================================================

rule all:
    input:
        target_plots,
        target_tables
        # target_bedgraphs,
        # target_bigwigs


rule genome_coverage:
    input:
        indexes = config["indexes"],
        target_genome = config["target_genome"],
    output:
        svg = config["output_prefix"] + "/{contig}.svg",
        table = config["output_prefix"] + "/{contig}.tsv"
    conda:
        "config/requirements.yml"
    params:
        contig = "{contig}",
        groups = config["groups"],
        title = config["target_genome"],
        legend_title = config["legend_title"],
        color_palette = config["color_palette"]
    resources:
        mem_mb = get_mem_mb,
        disk_mb = get_disk_mb
    threads:
        config["threads"]
    retries: 4
    script:
        "scripts/genome_coverage.py"


# rule coverage_bedgraph:
#     input:
#         indexes = config["indexes"],
#         target_genome = config["target_genome"],
#     output:
#         bdg = [config["output_prefix"] + "/{contig}_" + parent + ".bdg.gz"
#                 for parent in config["groups"]]
#     conda:
#         "config/requirements.yml"
#     params:
#         contig = "{contig}",
#         contig_size = get_contig_size
#     resources:
#         mem_mb = get_mem_mb,
#         disk_mb = get_mem_mb
#     # retries: 4
#     script:
#         "scripts/coverage_bedgraph.py"


# rule bedgraph_to_bigwig:
#     input:
#         bdg = config["output_prefix"] + "/{contig}_{parent}.bdg.gz",
#         target_genome = config["target_genome"],
#     output:
#         bw = config["output_prefix"] + "/{contig}_{parent}.bw"
#     conda:
#         "config/requirements.yml"
#     resources:
#         mem_mb = get_mem_mb,
#         disk_mb = get_mem_mb
#     # retries: 4
#     shell:
#         """
#         faidx -i chromsizes {input.target_genome} > {input.target_genome}.chrom.sizes
#         gunzip -c {input.bdg} | sortBed > {wildcards.contig}_{wildcards.parent}.bdg
#         bedGraphToBigWig {wildcards.contig}_{wildcards.parent}.bdg {input.target_genome}.chrom.sizes {output.bw}
#         """
