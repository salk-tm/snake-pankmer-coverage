from PanKmer import PKResults, genome_coverage
# genome_coverage(*(PKResults(i) for i in snakemake.input.indexes),
#     ref=snakemake.input.target_genome,
#     chromosomes=[snakemake.params.contig],
#     groups=snakemake.params.groups,
#     output=snakemake.output.svg,
#     title=snakemake.params.title,
#     legend=True,
#     legend_title='Parent',
#     legend_loc='outside',
#     alpha=1.0,
#     linewidth=2
# )

genome_coverage(*(PKResults(i) for i in snakemake.input.indexes),
    ref=snakemake.input.target_genome,
    chromosomes=[snakemake.params.contig],
    output=snakemake.output.svg,
    output_table=snakemake.output.table,
    groups=snakemake.params.groups,
    title=snakemake.params.title,
    legend=True,
    legend_title=snakemake.params.legend_title,
    legend_loc='outside',
    color_palette=snakemake.params.color_palette,
    processes=snakemake.threads
)